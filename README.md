# dd-mud-dungeon-master-service

Spring Projekt für den Dungeon-Master. Hat eine REST-Schnittstelle und Kafka-Anbindung.

## Konzept
Über die REST-Schnittstelle kann der DM Änderungen am Dungeon mitteilen, während über Kafka, der Service mitgeteilt bekommt, wie das aktuelle Spiel aussieht
und dementsprechend an den Websocket-Service ständige Updates sendet.

## REST-Schnittstelle
* POST: Neuen Raum hinzufügen
* POST: Neue Aktion hinzufügen
* POST: Spieler aus Spiel ausschließen

## Kafka-Anbindung
* Listener: Bekommt relevante Aktualisierungen über Spiel mitgeteilt.
* Producer: Gibt Spielaktualisierung an den Spielservice weiter. 