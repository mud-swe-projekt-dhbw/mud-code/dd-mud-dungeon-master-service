package dd.mud.dungeonmaster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DdMudDungeonmasterServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DdMudDungeonmasterServiceApplication.class, args);
	}

}
