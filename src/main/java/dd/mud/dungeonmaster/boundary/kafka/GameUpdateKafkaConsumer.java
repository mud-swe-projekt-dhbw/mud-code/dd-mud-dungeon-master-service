package dd.mud.dungeonmaster.boundary.kafka;

import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import dd.mud.dungeonmaster.control.service.ReceiveGameUpdateService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameUpdateKafkaConsumer {

    private final ReceiveGameUpdateService receiveGameUpdateService;

    /**
     * Takes updated Compact Game form game service
     * @param compactGame compact game object
     */
    @KafkaListener(topics = "dungeonMasterCurrentGame.t", groupId = "dmconsumergroup")
    public void getCurrentGameFromGameService (CompactGame compactGame) {
        this.receiveGameUpdateService.receiveNewGameUpdate(compactGame);
    }
}
