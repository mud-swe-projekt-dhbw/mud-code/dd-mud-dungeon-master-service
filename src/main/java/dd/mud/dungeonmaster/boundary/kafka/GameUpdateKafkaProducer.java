package dd.mud.dungeonmaster.boundary.kafka;

import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameUpdateKafkaProducer {

    private static final String GAME_SERVICE_TOPIC = "gameDMCurrentGame.t";

    private final KafkaTemplate<String, CompactGame> kafkaTemplate;

    /**
     * Takes compact game and sends it to game service
     * @param compactGame compact game object
     */
    public void sendMessageToGameService (CompactGame compactGame) {
        this.kafkaTemplate.send(GAME_SERVICE_TOPIC, compactGame);
    }
}
