package dd.mud.dungeonmaster.boundary.kafka;

import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class WebsocketUpdateKafkaProducer {

    private static final String WEBSOCKET_TOPIC = "dungeonMasterAnswer.t";

    private final KafkaTemplate<String, CompactGame> kafkaTemplate;

    /**
     * Takes compact game and sends it to websocket service
     * @param compactGame compact game object
     */
    public void sendMessageToClientWebsocket(CompactGame compactGame) {
        this.kafkaTemplate.send(WEBSOCKET_TOPIC, compactGame);
    }
}
