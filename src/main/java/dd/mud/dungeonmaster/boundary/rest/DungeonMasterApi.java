package dd.mud.dungeonmaster.boundary.rest;

import dd.mud.dungeonmaster.boundary.rest.dto.ActionTO;
import dd.mud.dungeonmaster.boundary.rest.dto.DescriptionTO;
import dd.mud.dungeonmaster.boundary.rest.dto.RoomDescriptionTO;
import dd.mud.dungeonmaster.boundary.rest.dto.RoomTO;
import dd.mud.dungeonmaster.control.aggregator.BanPlayerAggregator;
import dd.mud.dungeonmaster.control.aggregator.UpdateRoomDescriptionAggregator;
import dd.mud.dungeonmaster.control.aggregator.NewActionAggregator;
import dd.mud.dungeonmaster.control.aggregator.NewRoomAggregator;
import dd.mud.dungeonmaster.control.service.InformationGathererService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST-Controller for DM-Service
 */
@RestController
@RequestMapping(value = "/dungeonmaster")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DungeonMasterApi {

    private final BanPlayerAggregator banPlayerAggregator;
    private final NewActionAggregator newActionAggregator;
    private final NewRoomAggregator newRoomAggregator;
    private final UpdateRoomDescriptionAggregator updateRoomDescriptionAggregator;
    private final InformationGathererService informationGathererService;

    /**
     * Takes new Room DTO and delegates it to aggregator
     * @param playerId player who send message
     * @param gameId corresponding game id
     * @param newRoom room object
     * @return HTTP 200
     */
    @PostMapping(value = "/player/{player-id}/game/{game-id}/room")
    public ResponseEntity<Void> postNewRoom(@PathVariable("player-id") Long playerId,
                                            @PathVariable("game-id") Long gameId,
                                            @RequestBody RoomTO newRoom) {

        this.newRoomAggregator.processNewRoom(newRoom, gameId, playerId);

        return ResponseEntity.ok().build();
    }

    /**
     * Takes new action DTO and delegates it to aggregator
     * @param playerId player who send message
     * @param gameId corresponding game id
     * @param newAction action object
     * @return HTTP 200
     */
    @PostMapping(value = "/player/{player-id}/game/{game-id}/action")
    public ResponseEntity<Void> postNewAction(@PathVariable("player-id") Long playerId,
                                              @PathVariable("game-id") Long gameId,
                                              @RequestBody ActionTO newAction) {

        this.newActionAggregator.processNewAction(newAction, gameId, playerId);

        return ResponseEntity.ok().build();
    }

    /**
     * Takes id from player who should be banned and delegates task
     * @param playerId player who send message
     * @param gameId corresponding game id
     * @param playerBanId player to ban
     * @return HTTP 200
     */
    @PostMapping(value = "/player/{player-id}/game/{game-id}/ban/{player-ban-id}")
    public ResponseEntity<Void> postPlayerBan(@PathVariable("player-id") Long playerId,
                                              @PathVariable("game-id") Long gameId,
                                              @PathVariable("player-ban-id") Long playerBanId) {

        this.banPlayerAggregator.banPlayer(gameId, playerId, playerBanId);

        return ResponseEntity.ok().build();
    }

    /**
     * Takes description object
     * @param playerId player who send message
     * @param gameId corresponding game id
     * @param roomId room who gets new description
     * @param roomDescriptionTO description object
     * @return HTTP 200
     */
    @PutMapping(value = "/player/{player-id}/game/{game-id}/room/{room-id}/new-description")
    public ResponseEntity<Void> putNewRoomDescription(@PathVariable("player-id") Long playerId,
                                                      @PathVariable("game-id") Long gameId,
                                                      @PathVariable("room-id") Long roomId,
                                                      @RequestBody RoomDescriptionTO roomDescriptionTO) {

        this.updateRoomDescriptionAggregator.updateRoomDescription(playerId, gameId, roomId, roomDescriptionTO);

        return ResponseEntity.ok().build();
    }

    /**
     * REST-GET-Call for getting essential game information for DM
     * @param playerId player who send message
     * @param gameId corresponding game id
     * @return HTTP 200 and information object
     */
    @GetMapping(value = "/player/{player-id}/game/{game-id}/information")
    public ResponseEntity<DescriptionTO> getGameInformation (@PathVariable("player-id") Long playerId,
                                                             @PathVariable("game-id") Long gameId) {

        return ResponseEntity.ok(informationGathererService.getInformation(playerId, gameId));
    }
}
