package dd.mud.dungeonmaster.boundary.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ActionTO {

    private String actionName;
    private String actionResult;
    private Long roomId;
    private Boolean isGlobal;
}
