package dd.mud.dungeonmaster.boundary.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DescriptionTO {
    String information;
    List<ClassTO> classTOS;
    List<RaceTO> raceTOS;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class ClassTO {
        Long classId;
        String className;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class RaceTO {
        Long raceId;
        String raceName;
    }
}
