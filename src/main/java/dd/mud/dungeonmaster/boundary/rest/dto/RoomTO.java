package dd.mud.dungeonmaster.boundary.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RoomTO {
    private String roomName;
    private Long x;
    private Long y;
    private String description;
    private List<ActionRoomCreationTO> actions;
    private List<ItemTO> items;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class ItemTO {
        private String itemType;
        private String itemName;
        private Long itemValue;
    }
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class ActionRoomCreationTO {
        private String actionName;
        private String actionResult;
    }
}
