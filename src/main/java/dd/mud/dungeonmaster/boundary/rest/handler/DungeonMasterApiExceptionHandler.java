package dd.mud.dungeonmaster.boundary.rest.handler;

import dd.mud.dungeonmaster.control.exception.GameNotExsistentException;
import dd.mud.dungeonmaster.control.exception.PlayerDoesNotExsistException;
import dd.mud.dungeonmaster.control.exception.PlayerHasNoPermissionException;
import dd.mud.dungeonmaster.control.exception.RoomNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Catches exceptions for REST-Controller and returns corresponding HTTP answers.
 */
@RestControllerAdvice
public class DungeonMasterApiExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Void> handleUnexpectedException(Exception e) {
        e.printStackTrace();
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(PlayerHasNoPermissionException.class)
    public ResponseEntity<Void> handleUnexpectedException(PlayerHasNoPermissionException e) {
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(RoomNotFoundException.class)
    public ResponseEntity<Void> handleUnexpectedException(RoomNotFoundException e) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(GameNotExsistentException.class)
    public ResponseEntity<Void> handleUnexpectedException(GameNotExsistentException e) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(PlayerDoesNotExsistException.class)
    public ResponseEntity<Void> handleUnexpectedException(PlayerDoesNotExsistException e) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
