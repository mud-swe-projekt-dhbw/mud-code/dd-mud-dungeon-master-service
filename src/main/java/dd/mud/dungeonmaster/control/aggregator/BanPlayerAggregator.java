package dd.mud.dungeonmaster.control.aggregator;

import dd.mud.dungeonmaster.control.service.BanPlayerService;
import dd.mud.dungeonmaster.control.service.NotifyGameChangeService;
import dd.mud.dungeonmaster.control.service.PermissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class BanPlayerAggregator {

    private final PermissionService permissionService;
    private final BanPlayerService banPlayerService;
    private final NotifyGameChangeService notifyGameChangeService;

    /**
     * Aggregator for player ban. Connects services
     * @param gameId game id
     * @param playerId player id
     * @param playerToBanId player to ban id
     */
    public void banPlayer (Long gameId, Long playerId, Long playerToBanId) {
        this.permissionService.checkPermissionOnGame(playerId, gameId);
        this.banPlayerService.banPlayerAndDelete(gameId, playerToBanId);
        this.notifyGameChangeService.notifyGameChanged(gameId);
    }
}
