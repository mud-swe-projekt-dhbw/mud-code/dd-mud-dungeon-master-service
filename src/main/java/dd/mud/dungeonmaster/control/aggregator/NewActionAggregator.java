package dd.mud.dungeonmaster.control.aggregator;

import dd.mud.dungeonmaster.boundary.rest.dto.ActionTO;
import dd.mud.dungeonmaster.control.service.NotifyGameChangeService;
import dd.mud.dungeonmaster.control.service.PermissionService;
import dd.mud.dungeonmaster.control.service.SaveNewActionService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class NewActionAggregator {

    private final PermissionService permissionService;
    private final SaveNewActionService saveNewActionService;
    private final NotifyGameChangeService notifyGameChangeService;

    /**
     * Connects services for new action workflow
     * @param actionTO action object
     * @param gameId game id
     * @param playerId player id
     */
    public void processNewAction (ActionTO actionTO, Long gameId, Long playerId) {
        this.permissionService.checkPermissionOnGame(playerId, gameId);
        this.saveNewActionService.saveNewActionToCompactGame(gameId, actionTO);
        this.saveNewActionService.saveNewActionToDatabase(gameId, actionTO);
        this.notifyGameChangeService.notifyGameChanged(gameId);
    }
}
