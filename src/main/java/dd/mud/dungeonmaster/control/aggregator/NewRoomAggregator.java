package dd.mud.dungeonmaster.control.aggregator;

import dd.mud.dungeonmaster.boundary.rest.dto.RoomTO;
import dd.mud.dungeonmaster.control.service.NotifyGameChangeService;
import dd.mud.dungeonmaster.control.service.PermissionService;
import dd.mud.dungeonmaster.control.service.SaveNewRoomService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class NewRoomAggregator {

    private final PermissionService permissionService;
    private final NotifyGameChangeService notifyGameChangeService;
    private final SaveNewRoomService saveNewRoomService;

    /**
     * Connects services for new room workflow
     * @param roomTO room object
     * @param gameId game id
     * @param playerId player id
     */
    public void processNewRoom (RoomTO roomTO, Long gameId, Long playerId) {
        this.permissionService.checkPermissionOnGame(playerId, gameId);
        Long newRoomId = this.saveNewRoomService.saveRoomToDatabaseAndReturnNewRoomId(gameId, roomTO);
        this.saveNewRoomService.saveRoomToCompactGame(gameId, newRoomId, roomTO);
        this.notifyGameChangeService.notifyGameChanged(gameId);
    }
}
