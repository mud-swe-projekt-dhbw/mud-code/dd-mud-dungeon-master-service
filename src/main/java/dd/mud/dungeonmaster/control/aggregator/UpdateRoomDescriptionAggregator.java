package dd.mud.dungeonmaster.control.aggregator;

import dd.mud.dungeonmaster.boundary.rest.dto.RoomDescriptionTO;
import dd.mud.dungeonmaster.control.service.NotifyGameChangeService;
import dd.mud.dungeonmaster.control.service.PermissionService;
import dd.mud.dungeonmaster.control.service.UpdateRoomDescriptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class UpdateRoomDescriptionAggregator {

    private final PermissionService permissionService;
    private final UpdateRoomDescriptionService updateRoomDescriptionService;
    private final NotifyGameChangeService notifyGameChangeService;

    /**
     * Connects services for update room description workflow
     * @param playerId player id
     * @param gameId game id
     * @param roomId room id
     * @param roomDescriptionTO room description object
     */
    public void updateRoomDescription (Long playerId, Long gameId, Long roomId, RoomDescriptionTO roomDescriptionTO) {
        this.permissionService.checkPermissionOnGame(playerId, gameId);
        this.updateRoomDescriptionService
                .updateRoomDescriptionInCompactGame(gameId, roomId, roomDescriptionTO.getDescription());
        this.updateRoomDescriptionService
                .updateRoomDescriptionInDatabase(gameId, roomId, roomDescriptionTO.getDescription());
        this.notifyGameChangeService.notifyGameChanged(gameId);
    }
}
