package dd.mud.dungeonmaster.control.game;

import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class DMGameHolder {

    @Getter
    public Map<Long, CompactGame> cachedGames = new HashMap<>();
}
