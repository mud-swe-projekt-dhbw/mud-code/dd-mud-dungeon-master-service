package dd.mud.dungeonmaster.control.mapper;

import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import dd.mud.dungeonmaster.boundary.rest.dto.ActionTO;
import dd.mud.dungeonmaster.entity.model.GameAction;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ActionMapper {

    public static GameAction mapTOtoEntity (Long gameId, ActionTO actionTO) {
        if (actionTO.getIsGlobal().equals(Boolean.TRUE)) {
            return GameAction.builder()
                    .actionName(actionTO.getActionName())
                    .actionResult(actionTO.getActionResult())
                    .roomId(actionTO.getRoomId())
                    .isGlobal(actionTO.getIsGlobal())
                    .gameId(null)
                    .build();
        } else {
            return GameAction.builder()
                    .actionName(actionTO.getActionName())
                    .actionResult(actionTO.getActionResult())
                    .roomId(actionTO.getRoomId())
                    .isGlobal(actionTO.getIsGlobal())
                    .gameId(gameId)
                    .build();
        }
    }

    public static CompactGame.CompactAction mapTOtoCompactObject (ActionTO actionTO) {
        return CompactGame.CompactAction.builder()
                .actionName(actionTO.getActionName())
                .actionResult(actionTO.getActionResult())
                .build();
    }
}
