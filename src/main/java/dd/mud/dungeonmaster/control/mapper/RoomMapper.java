package dd.mud.dungeonmaster.control.mapper;

import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import dd.mud.dungeonmaster.boundary.rest.dto.RoomTO;
import dd.mud.dungeonmaster.entity.model.GameAction;
import dd.mud.dungeonmaster.entity.model.Item;
import dd.mud.dungeonmaster.entity.model.Room;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RoomMapper {

    private static final Boolean DEFAULT_IS_GLOBAL_VALUE = Boolean.FALSE;

    public Room mapRoomTOToRoomEntity (RoomTO roomTO, Long gameId) {
        return Room.builder()
                .coordx(roomTO.getX())
                .coordy(roomTO.getY())
                .descrip(roomTO.getDescription())
                .gameId(gameId)
                .roomName(roomTO.getRoomName())
                .build();
    }

    public List<GameAction> getListOfEntityActionsFromActionTOs (Long gameId, List<RoomTO.ActionRoomCreationTO> actionTOs, Long roomId) {
        return actionTOs.stream().map(actionTO -> GameAction.builder()
                .roomId(roomId)
                .actionResult(actionTO.getActionResult())
                .actionName(actionTO.getActionName())
                .isGlobal(DEFAULT_IS_GLOBAL_VALUE)
                .gameId(gameId)
                .build())
                .collect(Collectors.toList());
    }

    public List<Item> getListOfEntityItemsFromItemTOs (List<RoomTO.ItemTO> itemTOs, Long gameId) {
        return itemTOs.stream().map(itemTO -> Item.builder()
                .gameId(gameId)
                .itemType(itemTO.getItemType())
                .itemValue(itemTO.getItemValue())
                .itemName(itemTO.getItemName())
                .build())
                .collect(Collectors.toList());
    }

    public CompactGame.CompactRoom mapTOtoCompactObject (RoomTO roomTO, Long newId) {
        return CompactGame.CompactRoom.builder()
                .charactersInRoom(new ArrayList<>())
                .description(roomTO.getDescription())
                .coordx(roomTO.getX().intValue())
                .coordy(roomTO.getY().intValue())
                .roomName(roomTO.getRoomName())
                .roomId(newId)
                .actions(roomTO.getActions().stream().map(this::mapCompactAction).collect(Collectors.toList()))
                .compactItems(roomTO.getItems().stream().map(this::mapCompactItem).collect(Collectors.toList()))
                .build();
    }

    private CompactGame.CompactAction mapCompactAction (RoomTO.ActionRoomCreationTO actionTO) {
        return CompactGame.CompactAction.builder()
                .actionResult(actionTO.getActionResult())
                .actionName(actionTO.getActionName())
                .build();
    }

    private CompactGame.CompactRoom.CompactItem mapCompactItem (RoomTO.ItemTO itemTO) {
        return CompactGame.CompactRoom.CompactItem.builder()
                .itemName(itemTO.getItemName())
                .itemType(itemTO.getItemType())
                .itemValue(itemTO.getItemValue())
                .build();
    }
}
