package dd.mud.dungeonmaster.control.service;

import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import dd.mud.dungeonmaster.control.exception.PlayerDoesNotExsistException;
import dd.mud.dungeonmaster.control.game.DMGameHolder;
import dd.mud.dungeonmaster.entity.model.PlayerGameBan;
import dd.mud.dungeonmaster.entity.repository.PlayerGameBanRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class BanPlayerService {

    private final PlayerGameBanRepository playerGameBanRepository;
    private final DMGameHolder dmGameHolder;

    public void banPlayerAndDelete(Long gameId, Long playerToBanId) {
        CompactGame compactGame = this.dmGameHolder.getCachedGames().get(gameId);
        checkIfPlayerExsistsAndDelete(compactGame, playerToBanId);
        compactGame.getBannedPlayers().add(playerToBanId);
        this.playerGameBanRepository.save(PlayerGameBan.builder()
            .gameId(gameId)
            .playerId(playerToBanId)
            .build()
        );
    }

    private void checkIfPlayerExsistsAndDelete (CompactGame compactGame, Long playerToBanId) {
        boolean playerExsists = false;
        for (CompactGame.CompactRoom compactRoom : compactGame.getRooms()) {
            int index = 0;
            boolean playerFound = false;
            for (CompactGame.CompactRoom.CompactCharacter compactCharacter: compactRoom.getCharactersInRoom()) {
                if (compactCharacter.getPlayerId().equals(playerToBanId)) {
                    playerFound = true;
                    break;
                }
                index++;
            }
            if (playerFound) {
                playerExsists = true;
                compactRoom.getCharactersInRoom().remove(index);
                break;
            }
        }
        if (!playerExsists) throw new PlayerDoesNotExsistException();
    }
}
