package dd.mud.dungeonmaster.control.service;

import dd.mud.dungeonmaster.boundary.rest.dto.DescriptionTO;
import dd.mud.dungeonmaster.control.exception.GameNotExsistentException;
import dd.mud.dungeonmaster.entity.model.Ability;
import dd.mud.dungeonmaster.entity.model.Clazz;
import dd.mud.dungeonmaster.entity.model.ClazzAbility;
import dd.mud.dungeonmaster.entity.model.Game;
import dd.mud.dungeonmaster.entity.model.Race;
import dd.mud.dungeonmaster.entity.model.RaceAbility;
import dd.mud.dungeonmaster.entity.repository.AbilityRepository;
import dd.mud.dungeonmaster.entity.repository.ClazzAbilityRepository;
import dd.mud.dungeonmaster.entity.repository.ClazzRepository;
import dd.mud.dungeonmaster.entity.repository.GameRepository;
import dd.mud.dungeonmaster.entity.repository.RaceAbilityRepository;
import dd.mud.dungeonmaster.entity.repository.RaceRepository;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class InformationGathererService {

    private static final String GAME_INFO_HEADER = "Spiel-Informationen:\n";
    private static final String GAME_NAME = "Name: ";
    private static final String CLASSES_AND_ABILITIES = "Klassen und ihre Fertigkeiten:\n";
    private static final String RACES_AND_ABILITIES = "Rassen und ihre Fertigkeiten:\n";
    private static final String LINE_BREAK = "\n";
    private static final String COLON = ": ";
    private static final String COMMA_SPACE = ", ";

    private final ClazzAbilityRepository clazzAbilityRepository;
    private final ClazzRepository clazzRepository;
    private final RaceRepository raceRepository;
    private final RaceAbilityRepository raceAbilityRepository;
    private final GameRepository gameRepository;
    private final PermissionService permissionService;
    private final AbilityRepository abilityRepository;

    /**
     * Creates description DTO from game id
     * @param playerId player id of dm
     * @param gameId game id
     * @return description DTO object
     */
    public DescriptionTO getInformation (Long playerId, Long gameId) {
        this.permissionService.checkPermissionOnGame(playerId, gameId);

        // Gather infos
        Game game = this.gameRepository.findById(gameId).orElseThrow(GameNotExsistentException::new);
        List<Race> races = this.raceRepository.findAllByGameId(gameId);
        List<Clazz> clazzes = this.clazzRepository.findAllByGameId(gameId);
        List<RaceAbility> raceAbilities = this.raceAbilityRepository.findAllByGameId(gameId);
        List<ClazzAbility> clazzAbilities = this.clazzAbilityRepository.findAllByGameId(gameId);
        List<Ability> abilities = this.abilityRepository.findAllByGameId(gameId);

        // Assemble string
        String description = createDescriptionString(game, races, clazzes, raceAbilities, clazzAbilities, abilities);

        // Get races and classes
        List<DescriptionTO.ClassTO> classTOS = clazzes.stream().map(InformationGathererService::clazzEntityToClassTO)
                .collect(Collectors.toList());
        List<DescriptionTO.RaceTO> raceTOS = races.stream().map(InformationGathererService::raceEntityToRaceTO)
                .collect(Collectors.toList());

        return DescriptionTO.builder()
                .information(description)
                .classTOS(classTOS)
                .raceTOS(raceTOS)
                .build();
    }

    private static String createDescriptionString (Game game,
                                            List<Race> races,
                                            List<Clazz> clazzes,
                                            List<RaceAbility> raceAbilities,
                                            List<ClazzAbility> clazzAbilities,
                                            List<Ability> abilities) {

        StringBuilder gameInfoBuilder = new StringBuilder();
        gameInfoBuilder.append(GAME_INFO_HEADER).append(GAME_NAME).append(game.getGameName()).append(LINE_BREAK);

        List<LongStringTripel> longStringTripelsClasses = clazzAbilities.stream().map(l -> LongStringTripel.builder()
        .idObject(l.getClassId()).idAbility(l.getAbilityId()).build()).collect(Collectors.toList());
        longStringTripelsClasses.forEach(ls -> abilities.forEach(ability -> {
            if (ability.getId().equals(ls.getIdAbility())) {
                ls.setString(ability.getAbilityName());
            }
        }));

        List<LongStringTripel> longStringTripelRaces = raceAbilities.stream().map(l -> LongStringTripel.builder()
                .idObject(l.getRaceId()).idAbility(l.getAbilityId()).build()).collect(Collectors.toList());
        longStringTripelRaces.forEach(ls -> abilities.forEach(ability -> {
            if (ability.getId().equals(ls.getIdAbility())) {
                ls.setString(ability.getAbilityName());
            }
        }));

        gameInfoBuilder.append(LINE_BREAK).append(CLASSES_AND_ABILITIES);

        clazzes.forEach(clazz -> {
            gameInfoBuilder.append(clazz.getClassName()).append(COLON);
            longStringTripelsClasses.forEach(lc -> {
                if (lc.getIdObject().equals(clazz.getId())) gameInfoBuilder.append(lc.getString()).append(COMMA_SPACE);
            });
            gameInfoBuilder.append(LINE_BREAK);
        });

        gameInfoBuilder.append(LINE_BREAK).append(RACES_AND_ABILITIES);

        races.forEach(race -> {
            gameInfoBuilder.append(race.getRaceName()).append(COLON);
            longStringTripelRaces.forEach(lr -> {
                if (lr.getIdObject().equals(race.getId())) gameInfoBuilder.append(lr.getString()).append(COMMA_SPACE);
            });
            gameInfoBuilder.append(LINE_BREAK);
        });

        return gameInfoBuilder.toString();
    }

    private static DescriptionTO.ClassTO clazzEntityToClassTO (Clazz clazz) {
        return DescriptionTO.ClassTO.builder()
                .classId(clazz.getId())
                .className(clazz.getClassName())
                .build();
    }

    private static DescriptionTO.RaceTO raceEntityToRaceTO (Race race) {
        return DescriptionTO.RaceTO.builder()
                .raceId(race.getId())
                .raceName(race.getRaceName())
                .build();
    }

    @Builder
    @AllArgsConstructor
    @Getter
    @Setter
    private static class LongStringTripel {
        Long idObject;
        Long idAbility;
        String string;
    }
}
