package dd.mud.dungeonmaster.control.service;

import dd.mud.dungeonmaster.boundary.kafka.GameUpdateKafkaProducer;
import dd.mud.dungeonmaster.boundary.kafka.WebsocketUpdateKafkaProducer;
import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import dd.mud.dungeonmaster.control.game.DMGameHolder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class NotifyGameChangeService {

    private final DMGameHolder dmGameHolder;
    private final GameUpdateKafkaProducer gameUpdateKafkaProducer;
    private final WebsocketUpdateKafkaProducer websocketUpdateKafkaProducer;

    /**
     * Notifies other services about changes in the compact game!
     * @param gameId game id
     */
    public void notifyGameChanged (Long gameId) {
        CompactGame compactGame = this.dmGameHolder.getCachedGames().get(gameId);
        this.gameUpdateKafkaProducer.sendMessageToGameService(compactGame);
        this.websocketUpdateKafkaProducer.sendMessageToClientWebsocket(compactGame);
    }
}
