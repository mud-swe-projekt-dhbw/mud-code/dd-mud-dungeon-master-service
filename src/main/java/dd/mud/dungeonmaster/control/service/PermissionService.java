package dd.mud.dungeonmaster.control.service;

import dd.mud.dungeonmaster.control.exception.PlayerHasNoPermissionException;
import dd.mud.dungeonmaster.entity.model.Game;
import dd.mud.dungeonmaster.entity.repository.GameRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class PermissionService {

    private final GameRepository gameRepository;

    public void checkPermissionOnGame (Long playerId, Long gameId) {
        Game gameOptional = gameRepository.findById(gameId).orElseThrow(PlayerHasNoPermissionException::new);
        if (!gameOptional.getUserId().equals(playerId)) throw new PlayerHasNoPermissionException();
    }
}
