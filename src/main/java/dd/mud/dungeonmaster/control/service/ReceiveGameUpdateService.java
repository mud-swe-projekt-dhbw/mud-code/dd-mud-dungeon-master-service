package dd.mud.dungeonmaster.control.service;

import dd.mud.dungeonmaster.boundary.kafka.WebsocketUpdateKafkaProducer;
import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import dd.mud.dungeonmaster.control.game.DMGameHolder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ReceiveGameUpdateService {

    private final DMGameHolder dmGameHolder;
    private final WebsocketUpdateKafkaProducer websocketUpdateKafkaProducer;

    public void receiveNewGameUpdate (CompactGame compactGame) {
        if (dmGameHolder.getCachedGames().get(compactGame.getGameId()) == null) {
            this.dmGameHolder.getCachedGames().put(compactGame.getGameId(), compactGame);
        } else {
            this.dmGameHolder.getCachedGames().replace(compactGame.getGameId(), compactGame);
        }
        this.websocketUpdateKafkaProducer.sendMessageToClientWebsocket(compactGame);
    }
}
