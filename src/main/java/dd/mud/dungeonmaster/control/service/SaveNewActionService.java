package dd.mud.dungeonmaster.control.service;

import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import dd.mud.dungeonmaster.boundary.rest.dto.ActionTO;
import dd.mud.dungeonmaster.control.exception.RoomNotFoundException;
import dd.mud.dungeonmaster.control.game.DMGameHolder;
import dd.mud.dungeonmaster.control.mapper.ActionMapper;
import dd.mud.dungeonmaster.entity.model.GameAction;
import dd.mud.dungeonmaster.entity.repository.GameActionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SaveNewActionService {

    private final GameActionRepository gameActionRepository;
    private final DMGameHolder dmGameHolder;

    public void saveNewActionToCompactGame (Long gameId, ActionTO actionTO) {
        CompactGame.CompactAction compactAction = ActionMapper.mapTOtoCompactObject(actionTO);

        if (actionTO.getIsGlobal().equals(Boolean.TRUE)) {
            this.dmGameHolder.getCachedGames().get(gameId).getGlobalActions().add(compactAction);
        } else {
            this.dmGameHolder.getCachedGames().get(gameId).getRooms() // Get Rooms
                    .stream().filter(room -> room.getRoomId().equals(actionTO.getRoomId())) // Search for right room
                    .findFirst() // Get room
                    .orElseThrow(RoomNotFoundException::new) // Throw exception if room was not found
                    .getActions().add(compactAction); // Add action into found room
        }
    }

    public void saveNewActionToDatabase (Long gameId, ActionTO actionTO) {
        GameAction gameAction = ActionMapper.mapTOtoEntity(gameId, actionTO);
        this.gameActionRepository.save(gameAction);
    }
}
