package dd.mud.dungeonmaster.control.service;

import dd.mud.dungeonmaster.boundary.rest.dto.RoomTO;
import dd.mud.dungeonmaster.control.game.DMGameHolder;
import dd.mud.dungeonmaster.control.mapper.RoomMapper;
import dd.mud.dungeonmaster.entity.model.Item;
import dd.mud.dungeonmaster.entity.model.ItemRoom;
import dd.mud.dungeonmaster.entity.model.Room;
import dd.mud.dungeonmaster.entity.repository.GameActionRepository;
import dd.mud.dungeonmaster.entity.repository.ItemRepository;
import dd.mud.dungeonmaster.entity.repository.ItemRoomRepository;
import dd.mud.dungeonmaster.entity.repository.RoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SaveNewRoomService {

    private final RoomRepository roomRepository;
    private final ItemRoomRepository itemRoomRepository;
    private final ItemRepository itemRepository;
    private final GameActionRepository gameActionRepository;
    private final RoomMapper roomMapper;
    private final DMGameHolder dmGameHolder;

    /**
     * Saves new Room in database and returns new room id
     * @param gameId corresponding game id
     * @param roomTO incoming room TO
     * @return created room id
     */
    public Long saveRoomToDatabaseAndReturnNewRoomId (Long gameId, RoomTO roomTO) {
        Room newRoom = this.roomRepository.save(roomMapper.mapRoomTOToRoomEntity(roomTO, gameId));
        this.gameActionRepository.saveAll(this.roomMapper
                .getListOfEntityActionsFromActionTOs(gameId, roomTO.getActions(), newRoom.getId()));
        Iterable<Item> newItems = this.itemRepository.saveAll(this.roomMapper
                .getListOfEntityItemsFromItemTOs(roomTO.getItems(), gameId));
        this.itemRoomRepository.saveAll(createItemRoomList(gameId, newRoom.getId(), newItems));
        return newRoom.getId();
    }

    public void saveRoomToCompactGame (Long gameId, Long roomId, RoomTO roomTO) {
        this.dmGameHolder.getCachedGames().get(gameId).getRooms()
                .add(this.roomMapper.mapTOtoCompactObject(roomTO, roomId));
    }

    private static List<ItemRoom> createItemRoomList (Long gameId, Long roomId, Iterable<Item> items) {
        List<ItemRoom> itemRooms = new ArrayList<>();
        items.forEach(item -> itemRooms.add(ItemRoom.builder().itemId(item.getId()).roomId(roomId).gameId(gameId).build()));
        return itemRooms;
    }
}
