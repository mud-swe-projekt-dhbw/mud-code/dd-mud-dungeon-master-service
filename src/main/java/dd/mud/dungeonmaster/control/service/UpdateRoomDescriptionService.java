package dd.mud.dungeonmaster.control.service;

import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import dd.mud.dungeonmaster.control.exception.RoomNotFoundException;
import dd.mud.dungeonmaster.control.game.DMGameHolder;
import dd.mud.dungeonmaster.entity.model.Room;
import dd.mud.dungeonmaster.entity.repository.RoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class UpdateRoomDescriptionService {

    private final DMGameHolder dmGameHolder;
    private final RoomRepository roomRepository;

    public void updateRoomDescriptionInCompactGame (Long gameId, Long roomId, String roomDescription) {
        List<CompactGame.CompactRoom> compactRooms = this.dmGameHolder.getCachedGames().get(gameId).getRooms();
        compactRooms.forEach(compactRoom -> {
            if (compactRoom.getRoomId().equals(roomId)) {
                compactRoom.setDescription(roomDescription);
            }
        });
    }

    public void updateRoomDescriptionInDatabase (Long gameId, Long roomId, String roomDescription) {
        Room currentRoom = this.roomRepository.findById(roomId).orElseThrow(RoomNotFoundException::new);
        currentRoom.setDescrip(roomDescription);
        this.roomRepository.save(currentRoom);
    }
}
