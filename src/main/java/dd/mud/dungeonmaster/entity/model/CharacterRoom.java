package dd.mud.dungeonmaster.entity.model;

import dd.mud.dungeonmaster.entity.model.combinedkey.CharakterRoomKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@Table(name = "charakter_room", schema = "public")
@IdClass(CharakterRoomKey.class)
public class CharacterRoom implements Serializable {

    @Id
    Long charakterId;

    @Id
    Long roomId;

    Long gameId;
}
