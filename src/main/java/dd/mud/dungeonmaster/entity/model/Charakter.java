package dd.mud.dungeonmaster.entity.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@Table(name = "charakter", schema = "public")
public class Charakter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String chName;

    private String chLevel;

    private Long hp;

    private Long damage;

    private Long classId;

    private Long raceId;

    private Long userId;

    private Long gameId;

    private String description;

    private String personality;
}
