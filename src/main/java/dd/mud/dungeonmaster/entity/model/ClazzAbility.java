package dd.mud.dungeonmaster.entity.model;

import dd.mud.dungeonmaster.entity.model.combinedkey.ClazzAbilityKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@IdClass(ClazzAbilityKey.class)
@Table(name = "classAbility", schema = "public")
public class ClazzAbility {
    @Id
    Long classId;

    @Id
    Long abilityId;

    Long gameId;
}
