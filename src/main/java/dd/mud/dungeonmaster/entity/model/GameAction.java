package dd.mud.dungeonmaster.entity.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@Table(name = "gameaction", schema = "public")
public class GameAction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String actionName;

    String actionResult;

    Long roomId;

    Boolean isGlobal;

    @Nullable
    Long gameId;
}
