package dd.mud.dungeonmaster.entity.model;

import dd.mud.dungeonmaster.entity.model.combinedkey.ItemRoomKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@Table(name = "item_room", schema = "public")
@IdClass(ItemRoomKey.class)
public class ItemRoom implements Serializable {

    @Id
    Long roomId;

    @Id
    Long itemId;

    Long gameId;
}
