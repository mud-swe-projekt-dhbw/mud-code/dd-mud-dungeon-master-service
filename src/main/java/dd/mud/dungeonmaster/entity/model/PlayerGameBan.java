package dd.mud.dungeonmaster.entity.model;

import dd.mud.dungeonmaster.entity.model.combinedkey.PlayerGameBanKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@Table(name = "playergameban", schema = "public")
@IdClass(PlayerGameBanKey.class)
public class PlayerGameBan implements Serializable {

    @Id
    Long playerId;

    @Id
    Long gameId;
}
