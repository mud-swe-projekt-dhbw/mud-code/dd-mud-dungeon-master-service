package dd.mud.dungeonmaster.entity.model;

import dd.mud.dungeonmaster.entity.model.combinedkey.RaceAbilityKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@IdClass(RaceAbilityKey.class)
@Table(name = "raceAbility", schema = "public")
public class RaceAbility {
    @Id
    Long raceId;

    @Id
    Long abilityId;

    Long gameId;
}
