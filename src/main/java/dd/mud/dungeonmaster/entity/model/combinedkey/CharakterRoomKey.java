package dd.mud.dungeonmaster.entity.model.combinedkey;

import java.io.Serializable;

public class CharakterRoomKey implements Serializable {
    Long charakterId;
    Long roomId;
}
