package dd.mud.dungeonmaster.entity.model.combinedkey;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class ClazzAbilityKey implements Serializable {
    Long classId;

    Long abilityId;
}
