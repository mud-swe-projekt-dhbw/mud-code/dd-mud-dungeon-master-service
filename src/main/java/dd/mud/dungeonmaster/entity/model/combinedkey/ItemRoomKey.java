package dd.mud.dungeonmaster.entity.model.combinedkey;

import java.io.Serializable;

public class ItemRoomKey implements Serializable {
    Long roomId;
    Long itemId;
}
