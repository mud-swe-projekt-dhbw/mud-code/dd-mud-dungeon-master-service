package dd.mud.dungeonmaster.entity.model.combinedkey;

import java.io.Serializable;

public class PlayerGameBanKey implements Serializable {
    Long playerId;
    Long gameId;
}
