package dd.mud.dungeonmaster.entity.model.combinedkey;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class RaceAbilityKey implements Serializable {
    Long raceId;

    Long abilityId;
}
