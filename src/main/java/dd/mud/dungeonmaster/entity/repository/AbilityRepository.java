package dd.mud.dungeonmaster.entity.repository;

import dd.mud.dungeonmaster.entity.model.Ability;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AbilityRepository extends CrudRepository<Ability, Long> {
    List<Ability> findAllByGameId (Long gameId);
}
