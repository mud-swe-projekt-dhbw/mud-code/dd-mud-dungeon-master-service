package dd.mud.dungeonmaster.entity.repository;

import dd.mud.dungeonmaster.entity.model.CharacterRoom;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRoomRepository extends CrudRepository<CharacterRoom, Long> {
}
