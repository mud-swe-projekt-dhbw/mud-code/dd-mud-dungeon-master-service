package dd.mud.dungeonmaster.entity.repository;

import dd.mud.dungeonmaster.entity.model.Charakter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharakterRepository extends CrudRepository<Charakter, Long> {
}
