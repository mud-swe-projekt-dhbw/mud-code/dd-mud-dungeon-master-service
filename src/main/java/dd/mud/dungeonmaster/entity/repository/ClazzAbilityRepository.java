package dd.mud.dungeonmaster.entity.repository;

import dd.mud.dungeonmaster.entity.model.ClazzAbility;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClazzAbilityRepository extends CrudRepository<ClazzAbility, Long> {

    List<ClazzAbility> findAllByGameId (Long gameId);
}
