package dd.mud.dungeonmaster.entity.repository;

import dd.mud.dungeonmaster.entity.model.Clazz;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClazzRepository extends CrudRepository<Clazz, Long> {
    List<Clazz> findAllByGameId (Long gameId);
}
