package dd.mud.dungeonmaster.entity.repository;

import dd.mud.dungeonmaster.entity.model.GameAction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameActionRepository extends CrudRepository<GameAction, Long> {
}
