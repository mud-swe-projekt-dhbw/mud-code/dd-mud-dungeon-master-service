package dd.mud.dungeonmaster.entity.repository;

import dd.mud.dungeonmaster.entity.model.Game;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends CrudRepository<Game,Long> {
}
