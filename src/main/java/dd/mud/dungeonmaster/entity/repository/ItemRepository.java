package dd.mud.dungeonmaster.entity.repository;

import dd.mud.dungeonmaster.entity.model.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {
}
