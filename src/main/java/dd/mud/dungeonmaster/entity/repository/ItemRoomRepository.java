package dd.mud.dungeonmaster.entity.repository;

import dd.mud.dungeonmaster.entity.model.ItemRoom;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRoomRepository extends CrudRepository<ItemRoom, Long> {
}
