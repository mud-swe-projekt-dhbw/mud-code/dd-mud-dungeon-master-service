package dd.mud.dungeonmaster.entity.repository;

import dd.mud.dungeonmaster.entity.model.PlayerGameBan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerGameBanRepository extends CrudRepository<PlayerGameBan, Long> {
}
