package dd.mud.dungeonmaster.entity.repository;

import dd.mud.dungeonmaster.entity.model.RaceAbility;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RaceAbilityRepository extends CrudRepository<RaceAbility, Long> {
    List<RaceAbility> findAllByGameId (Long gameId);
}
