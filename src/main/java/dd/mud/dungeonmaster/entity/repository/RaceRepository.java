package dd.mud.dungeonmaster.entity.repository;

import dd.mud.dungeonmaster.entity.model.Race;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RaceRepository extends CrudRepository<Race, Long> {

    List<Race> findAllByGameId (Long gameId);
}
