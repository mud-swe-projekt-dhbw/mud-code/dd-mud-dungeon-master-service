package dd.mud.dungeonmaster.entity.repository;

import dd.mud.dungeonmaster.entity.model.Room;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends CrudRepository<Room, Long> {
}
