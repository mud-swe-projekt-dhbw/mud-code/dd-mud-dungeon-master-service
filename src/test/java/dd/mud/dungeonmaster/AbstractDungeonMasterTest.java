package dd.mud.dungeonmaster;

import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import dd.mud.dungeonmaster.boundary.rest.dto.ActionTO;
import dd.mud.dungeonmaster.boundary.rest.dto.DescriptionTO;
import dd.mud.dungeonmaster.boundary.rest.dto.RoomDescriptionTO;
import dd.mud.dungeonmaster.boundary.rest.dto.RoomTO;

import java.util.Collections;

public class AbstractDungeonMasterTest {


    protected CompactGame getCompactGame () {
        return CompactGame
                .builder()
                .gameId(1L)
                .dungeonMasterId(2L)
                .bannedPlayers(Collections.singletonList(3L))
                .globalActions(Collections.singletonList(CompactGame.CompactAction.builder().actionName("Name").actionResult("1;2;Text").build()))
                .rooms(
                        Collections.singletonList(
                                CompactGame.CompactRoom.builder()
                                        .actions(Collections.singletonList(CompactGame.CompactAction.builder().actionName("Name").actionResult("1;2;Text").build()))
                                        .coordy(0)
                                        .coordx(0)
                                        .compactItems(Collections.singletonList(CompactGame.CompactRoom.CompactItem.builder()
                                                .itemName("ItemName")
                                                .itemType("Type")
                                                .itemValue(12L)
                                                .build()))
                                        .description("Beschreibung")
                                        .roomId(90L)
                                        .roomName("RaumName")
                                        .charactersInRoom(Collections.singletonList(CompactGame.CompactRoom.CompactCharacter.builder()
                                                .characterItems(Collections.singletonList(CompactGame.CompactRoom.CompactItem.builder()
                                                        .itemName("ItemName")
                                                        .itemType("Type")
                                                        .itemValue(12L)
                                                        .build()))
                                                .classId(50L)
                                                .raceId(30L)
                                                .damage(100L)
                                                .id(45L)
                                                .description("ChBeschreiunb")
                                                .personality("Personalität")
                                                .hp(1000L)
                                                .name("Name")
                                                .playerId(500L)
                                                .userName("Username")
                                                .build()))
                                        .build()
                        )
                )
                .build();
    }

    protected ActionTO getActionTO () {
        return ActionTO.builder()
                .build();
    }

    protected DescriptionTO getDescriptionTO () {
        return DescriptionTO.builder().build();
    }

    protected RoomDescriptionTO getRoomDescriptionTO () {
        return RoomDescriptionTO.builder().description("Beschreibung").build();
    }

    protected RoomTO getRoomTO () {
        return RoomTO.builder().build();
    }
}
