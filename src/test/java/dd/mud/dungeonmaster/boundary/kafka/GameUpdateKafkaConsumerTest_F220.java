package dd.mud.dungeonmaster.boundary.kafka;

import dd.mud.dungeonmaster.AbstractDungeonMasterTest;
import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import dd.mud.dungeonmaster.control.service.ReceiveGameUpdateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class GameUpdateKafkaConsumerTest_F220 extends AbstractDungeonMasterTest {

    @Mock
    private ReceiveGameUpdateService receiveGameUpdateService;

    @InjectMocks
    private GameUpdateKafkaConsumer gameUpdateKafkaConsumer;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getCurrentGameFromGameService() {
        CompactGame compactGame = getCompactGame();
        doNothing().when(receiveGameUpdateService).receiveNewGameUpdate(compactGame);

        this.gameUpdateKafkaConsumer.getCurrentGameFromGameService(compactGame);

        verify(receiveGameUpdateService, times(1)).receiveNewGameUpdate(compactGame);
        verifyNoMoreInteractions(receiveGameUpdateService);
    }
}