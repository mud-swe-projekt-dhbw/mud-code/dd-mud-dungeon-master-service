package dd.mud.dungeonmaster.boundary.kafka;

import dd.mud.dungeonmaster.AbstractDungeonMasterTest;
import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.kafka.core.KafkaTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class GameUpdateKafkaProducerTest_F220 extends AbstractDungeonMasterTest {

    @Mock
    private KafkaTemplate<String, CompactGame> kafkaTemplate;

    @InjectMocks
    private GameUpdateKafkaProducer gameUpdateKafkaProducer;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void sendMessageToGameService() {
        CompactGame compactGame = getCompactGame();
        doReturn(null).when(kafkaTemplate).send("gameDMCurrentGame.t", compactGame);

        this.gameUpdateKafkaProducer.sendMessageToGameService(compactGame);

        verify(kafkaTemplate, times(1)).send("gameDMCurrentGame.t", compactGame);
        verifyNoMoreInteractions(kafkaTemplate);
    }
}