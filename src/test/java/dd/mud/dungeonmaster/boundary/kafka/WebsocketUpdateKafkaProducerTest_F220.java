package dd.mud.dungeonmaster.boundary.kafka;

import dd.mud.dungeonmaster.AbstractDungeonMasterTest;
import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.kafka.core.KafkaTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class WebsocketUpdateKafkaProducerTest_F220 extends AbstractDungeonMasterTest {

    @Mock
    private KafkaTemplate<String, CompactGame> kafkaTemplate;

    @InjectMocks
    private WebsocketUpdateKafkaProducer websocketUpdateKafkaProducer;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void sendMessageToClientWebsocket() {
        CompactGame compactGame = getCompactGame();
        doReturn(null).when(kafkaTemplate).send("dungeonMasterAnswer.t", compactGame);

        this.websocketUpdateKafkaProducer.sendMessageToClientWebsocket(compactGame);

        verify(kafkaTemplate, times(1)).send("dungeonMasterAnswer.t", compactGame);
        verifyNoMoreInteractions(kafkaTemplate);
    }
}