package dd.mud.dungeonmaster.boundary.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import dd.mud.dungeonmaster.AbstractDungeonMasterTest;
import dd.mud.dungeonmaster.boundary.rest.dto.DescriptionTO;
import dd.mud.dungeonmaster.control.aggregator.BanPlayerAggregator;
import dd.mud.dungeonmaster.control.aggregator.NewActionAggregator;
import dd.mud.dungeonmaster.control.aggregator.NewRoomAggregator;
import dd.mud.dungeonmaster.control.aggregator.UpdateRoomDescriptionAggregator;
import dd.mud.dungeonmaster.control.exception.GameNotExsistentException;
import dd.mud.dungeonmaster.control.exception.PlayerDoesNotExsistException;
import dd.mud.dungeonmaster.control.exception.PlayerHasNoPermissionException;
import dd.mud.dungeonmaster.control.exception.RoomNotFoundException;
import dd.mud.dungeonmaster.control.service.InformationGathererService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = DungeonMasterApi.class)
class DungeonMasterApiTest_F220 extends AbstractDungeonMasterTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BanPlayerAggregator banPlayerAggregator;

    @MockBean
    private NewActionAggregator newActionAggregator;

    @MockBean
    private NewRoomAggregator newRoomAggregator;

    @MockBean
    private UpdateRoomDescriptionAggregator updateRoomDescriptionAggregator;

    @MockBean
    private InformationGathererService informationGathererService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void postNewRoom() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        doNothing().when(newRoomAggregator).processNewRoom(any(), anyLong(), anyLong());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/dungeonmaster/player/1/game/2/room")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(getRoomTO()))
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilder).andExpect(status().isOk());
    }

    @Test
    void postNewRoom_noPermission_403() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        doThrow(PlayerHasNoPermissionException.class).when(newRoomAggregator).processNewRoom(any(), anyLong(), anyLong());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/dungeonmaster/player/1/game/2/room")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(getRoomTO()))
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilder).andExpect(status().isForbidden());
    }

    @Test
    void postNewAction() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        doNothing().when(newActionAggregator).processNewAction(any(), anyLong(), anyLong());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/dungeonmaster/player/1/game/2/action")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(getActionTO()))
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilder).andExpect(status().isOk());
    }

    @Test
    void postNewAction_noPermission_403() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        doThrow(PlayerHasNoPermissionException.class).when(newActionAggregator).processNewAction(any(), anyLong(), anyLong());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/dungeonmaster/player/1/game/2/action")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(getActionTO()))
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilder).andExpect(status().isForbidden());
    }

    @Test
    void postNewAction_roomNotFound_404() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        doThrow(RoomNotFoundException.class).when(newActionAggregator).processNewAction(any(), anyLong(), anyLong());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/dungeonmaster/player/1/game/2/action")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(getActionTO()))
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilder).andExpect(status().isNotFound());
    }

    @Test
    void postPlayerBan() throws Exception {
        doNothing().when(banPlayerAggregator).banPlayer(anyLong(), anyLong(), anyLong());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/dungeonmaster/player/2/game/3/ban/4")
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilder).andExpect(status().isOk());
    }

    @Test
    void postPlayerBan_noPermission_403() throws Exception {
        doThrow(PlayerHasNoPermissionException.class).when(banPlayerAggregator).banPlayer(anyLong(), anyLong(), anyLong());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/dungeonmaster/player/2/game/3/ban/4")
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilder).andExpect(status().isForbidden());
    }

    @Test
    void postPlayerBan_playerNotFound_404() throws Exception {
        doThrow(PlayerDoesNotExsistException.class).when(banPlayerAggregator).banPlayer(anyLong(), anyLong(), anyLong());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/dungeonmaster/player/2/game/3/ban/4")
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilder).andExpect(status().isNotFound());
    }

    @Test
    void putNewRoomDescription() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        doNothing().when(updateRoomDescriptionAggregator).updateRoomDescription(anyLong(), anyLong(), anyLong(), any());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/dungeonmaster/player/1/game/2/room/3/new-description")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(getRoomDescriptionTO()))
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilder).andExpect(status().isOk());
    }

    @Test
    void putNewRoomDescription_noPermission_403() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        doThrow(PlayerHasNoPermissionException.class).when(updateRoomDescriptionAggregator).updateRoomDescription(anyLong(), anyLong(), anyLong(), any());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/dungeonmaster/player/1/game/2/room/3/new-description")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(getRoomDescriptionTO()))
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilder).andExpect(status().isForbidden());
    }

    @Test
    void putNewRoomDescription_roomNotFound_404() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        doThrow(RoomNotFoundException.class).when(updateRoomDescriptionAggregator).updateRoomDescription(anyLong(), anyLong(), anyLong(), any());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/dungeonmaster/player/1/game/2/room/3/new-description")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(getRoomDescriptionTO()))
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilder).andExpect(status().isNotFound());
    }

    @Test
    void getGameInformation() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        DescriptionTO descriptionTO = getDescriptionTO();

        doReturn(descriptionTO).when(informationGathererService).getInformation(anyLong(), anyLong());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/dungeonmaster/player/1/game/2/information")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
        Assert.assertEquals(objectMapper.writeValueAsString(descriptionTO), mvcResult.getResponse().getContentAsString());
    }

    @Test
    void getGameInformation_noPermission_403() throws Exception {
        doThrow(PlayerHasNoPermissionException.class).when(informationGathererService).getInformation(anyLong(), anyLong());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/dungeonmaster/player/1/game/2/information")
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilder).andExpect(status().isForbidden());
    }

    @Test
    void getGameInformation_gameNotFound_404() throws Exception {
        doThrow(GameNotExsistentException.class).when(informationGathererService).getInformation(anyLong(), anyLong());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/dungeonmaster/player/1/game/2/information")
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilder).andExpect(status().isNotFound());
    }
}