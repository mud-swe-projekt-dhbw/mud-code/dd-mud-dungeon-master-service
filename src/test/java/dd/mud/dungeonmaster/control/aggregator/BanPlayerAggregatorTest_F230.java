package dd.mud.dungeonmaster.control.aggregator;

import dd.mud.dungeonmaster.AbstractDungeonMasterTest;
import dd.mud.dungeonmaster.control.service.BanPlayerService;
import dd.mud.dungeonmaster.control.service.NotifyGameChangeService;
import dd.mud.dungeonmaster.control.service.PermissionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
class BanPlayerAggregatorTest_F230 extends AbstractDungeonMasterTest {

    @Mock
    PermissionService permissionService;

    @Mock
    BanPlayerService banPlayerService;

    @Mock
    NotifyGameChangeService notifyGameChangeService;

    @InjectMocks
    BanPlayerAggregator banPlayerAggregator;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void banPlayer() {
        doNothing().when(permissionService).checkPermissionOnGame(anyLong(), anyLong());
        doNothing().when(banPlayerService).banPlayerAndDelete(anyLong(), anyLong());
        doNothing().when(notifyGameChangeService).notifyGameChanged(anyLong());

        this.banPlayerAggregator.banPlayer(1L, 2L, 3L);

        verify(permissionService, times(1)).checkPermissionOnGame(anyLong(), anyLong());
        verify(banPlayerService, times(1)).banPlayerAndDelete(anyLong(), anyLong());
        verify(notifyGameChangeService, times(1)).notifyGameChanged(anyLong());
    }
}