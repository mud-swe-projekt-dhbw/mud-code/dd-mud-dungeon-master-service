package dd.mud.dungeonmaster.control.aggregator;

import dd.mud.dungeonmaster.AbstractDungeonMasterTest;
import dd.mud.dungeonmaster.control.service.BanPlayerService;
import dd.mud.dungeonmaster.control.service.NotifyGameChangeService;
import dd.mud.dungeonmaster.control.service.PermissionService;
import dd.mud.dungeonmaster.control.service.SaveNewActionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class NewActionAggregatorTest_F220 extends AbstractDungeonMasterTest {

    @Mock
    private PermissionService permissionService;

    @Mock
    private SaveNewActionService saveNewActionService;

    @Mock
    private NotifyGameChangeService notifyGameChangeService;

    @InjectMocks
    private NewActionAggregator newActionAggregator;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void processNewAction() {
        doNothing().when(permissionService).checkPermissionOnGame(anyLong(), anyLong());
        doNothing().when(saveNewActionService).saveNewActionToCompactGame(anyLong(), any());
        doNothing().when(saveNewActionService).saveNewActionToDatabase(anyLong(), any());
        doNothing().when(notifyGameChangeService).notifyGameChanged(anyLong());

        this.newActionAggregator.processNewAction(getActionTO(), 1L, 2L);

        verify(permissionService, times(1)).checkPermissionOnGame(anyLong(), anyLong());
        verify(saveNewActionService, times(1)).saveNewActionToCompactGame(anyLong(), any());
        verify(saveNewActionService, times(1)).saveNewActionToDatabase(anyLong(), any());
        verify(notifyGameChangeService, times(1)).notifyGameChanged(anyLong());
    }
}