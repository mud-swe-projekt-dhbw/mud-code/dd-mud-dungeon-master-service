package dd.mud.dungeonmaster.control.aggregator;

import dd.mud.dungeonmaster.AbstractDungeonMasterTest;
import dd.mud.dungeonmaster.control.service.NotifyGameChangeService;
import dd.mud.dungeonmaster.control.service.PermissionService;
import dd.mud.dungeonmaster.control.service.SaveNewRoomService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class NewRoomAggregatorTest_F220 extends AbstractDungeonMasterTest {

    @Mock
    private PermissionService permissionService;

    @Mock
    private NotifyGameChangeService notifyGameChangeService;

    @Mock
    private SaveNewRoomService saveNewRoomService;

    @InjectMocks
    private NewRoomAggregator newRoomAggregator;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void processNewRoom() {
        doNothing().when(permissionService).checkPermissionOnGame(anyLong(), anyLong());
        doReturn(5L).when(saveNewRoomService).saveRoomToDatabaseAndReturnNewRoomId(anyLong(), any());
        doNothing().when(saveNewRoomService).saveRoomToCompactGame(anyLong(), eq(5L), any());
        doNothing().when(notifyGameChangeService).notifyGameChanged(anyLong());

        this.newRoomAggregator.processNewRoom(getRoomTO(), 1L, 2L);

        verify(permissionService, times(1)).checkPermissionOnGame(anyLong(), anyLong());
        verify(saveNewRoomService, times(1)).saveRoomToDatabaseAndReturnNewRoomId(anyLong(), any());
        verify(saveNewRoomService, times(1)).saveRoomToCompactGame(anyLong(), eq(5L), any());
        verify(notifyGameChangeService, times(1)).notifyGameChanged(anyLong());
    }
}