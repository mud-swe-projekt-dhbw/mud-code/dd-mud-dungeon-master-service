package dd.mud.dungeonmaster.control.aggregator;

import dd.mud.dungeonmaster.AbstractDungeonMasterTest;
import dd.mud.dungeonmaster.control.service.NotifyGameChangeService;
import dd.mud.dungeonmaster.control.service.PermissionService;
import dd.mud.dungeonmaster.control.service.UpdateRoomDescriptionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class UpdateRoomDescriptionAggregatorTest_F220 extends AbstractDungeonMasterTest {

    @Mock
    private PermissionService permissionService;

    @Mock
    private UpdateRoomDescriptionService updateRoomDescriptionService;

    @Mock
    private NotifyGameChangeService notifyGameChangeService;

    @InjectMocks
    private UpdateRoomDescriptionAggregator updateRoomDescriptionAggregator;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void updateRoomDescription() {
        doNothing().when(permissionService).checkPermissionOnGame(anyLong(), anyLong());
        doNothing().when(updateRoomDescriptionService).updateRoomDescriptionInCompactGame(anyLong(), anyLong(), anyString());
        doNothing().when(updateRoomDescriptionService).updateRoomDescriptionInDatabase(anyLong(), anyLong(), anyString());
        doNothing().when(notifyGameChangeService).notifyGameChanged(anyLong());

        this.updateRoomDescriptionAggregator.updateRoomDescription(1L, 2L, 3L, getRoomDescriptionTO());

        verify(permissionService, times(1)).checkPermissionOnGame(anyLong(), anyLong());
        verify(updateRoomDescriptionService, times(1)).updateRoomDescriptionInCompactGame(anyLong(), anyLong(), anyString());
        verify(updateRoomDescriptionService, times(1)).updateRoomDescriptionInDatabase(anyLong(), anyLong(), anyString());
        verify(notifyGameChangeService, times(1)).notifyGameChanged(anyLong());
    }
}