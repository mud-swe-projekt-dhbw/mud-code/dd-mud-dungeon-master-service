package dd.mud.dungeonmaster.control.service;

import dd.mud.dungeonmaster.AbstractDungeonMasterTest;
import dd.mud.dungeonmaster.boundary.kafka.messageobject.CompactGame;
import dd.mud.dungeonmaster.control.exception.PlayerHasNoPermissionException;
import dd.mud.dungeonmaster.entity.model.Game;
import dd.mud.dungeonmaster.entity.repository.GameRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class PermissionServiceTest_F220 extends AbstractDungeonMasterTest {

    @Mock
    private GameRepository gameRepository;

    @InjectMocks
    private PermissionService permissionService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void checkPermissionOnGame() {
        Game game = Game.builder().userId(1L).build();
        doReturn(Optional.of(game)).when(gameRepository).findById(anyLong());

        this.permissionService.checkPermissionOnGame(1L, 5L);

        verify(gameRepository, times(1)).findById(anyLong());
        verifyNoMoreInteractions(gameRepository);
    }

    @Test
    void checkPermissionOnGame_noPermission() {
        Game game = Game.builder().userId(3L).build();
        doReturn(Optional.of(game)).when(gameRepository).findById(anyLong());

        assertThrows(PlayerHasNoPermissionException.class,
                () -> this.permissionService.checkPermissionOnGame(1L, 5L));

        verify(gameRepository, times(1)).findById(anyLong());
        verifyNoMoreInteractions(gameRepository);
    }
}
